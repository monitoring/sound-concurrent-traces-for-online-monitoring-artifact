This repository contains artifacts for the paper Sound Concurrent Traces for Online Monitoring to appear in SPIN2023
-  ./facts contains the source code for FACTs and the experiment in Section 7
-  ./monitoring also contains the monitorability experiment in Section 7
- ./casualdependence contains the experiment where we extract the causal dependence from 55 event-based property specifications from Dweyer patterns written as Quantified Regular Expressions (QREs).