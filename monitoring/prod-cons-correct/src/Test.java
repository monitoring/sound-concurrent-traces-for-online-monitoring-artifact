public class Test {

    public static void main(String[] args) {

        int nWriters = Integer.parseInt(args[0]);
        int nReaders = Integer.parseInt(args[1]);
        int nWrites = Integer.parseInt(args[2]);
        int nReads = Integer.parseInt(args[3]);
        int arraySize = Integer.parseInt(args[4]);
        SharedVar var = new SharedVar(arraySize);

        Runnable[] runs = new Runnable[nReaders + nWriters];
        int i;
        for (i = 0; i < nWriters; i++)
            runs[i] = new Writer(var, nWrites);

        for (int j = 0; j < nReaders; j++)
            runs[i + j] = new Reader(var, nReads);

        Harness bench = new Harness(nReaders + nWriters);
        bench.start(runs);
        bench.end();
        System.out.println("Semaphore done");

    }

    public static void observe(String name, long thread, Object resource) {
        System.out.println("Instrumented call");
    }

}
