#!/bin/bash
DEFDRIVER=`cat driver`
TARGET=${2:-jmoprun}
DRIVER=${3:-$DEFDRIVER}
t=0
f=0
d=0
u=0
for i in $(seq 1 $1); do
	echo -n "$i "
	v=`gtimeout 1 make -s  ${TARGET} DRIVER="${DRIVER}" 2>&1`
	echo $v >> ${TARGET}.log
	if [[ -z $v ]]; then
		echo "Deadlock"
		d=$((d + 1))
	elif [[ $v = *"Failed!"* ]]; then
		echo "Incorrect"
#		echo $v
		f=$((f + 1))
	elif [[ $v = *"Complete"* ]]; then
		echo "Correct"
		t=$((t + 1))
	elif [[ $v = *"Error 143"* ]]; then
		echo "Deadlock"
		d=$((d + 1))
	else
		echo "Unknown $v"
		u=$((u + 1))
	fi
done
echo "T:$t F:$f Deadlock:$d U:$u"
