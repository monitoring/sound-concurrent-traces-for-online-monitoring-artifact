import java.util.Random;

public class Reader implements Runnable {
    private SharedVar var;
    private int nReads = 0;
    private Random r = new Random();

    public Reader(SharedVar var, int nReads) {
        this.var = var;
        this.nReads = nReads;
    }

    @Override
    public void run() {
        try {
            for (int i = 0; i < nReads; i++) {

                if(i % 3 != 0){
                var.service.acquire();

                var.counter.acquire();

                if (var.count == 0)
                    var.resource.acquire();
                var.count++;

                  var.service.release();
                var.counter.release();
                }

                int s = var.readArr();
 
                var.counter.acquire();

                var.count--;
                if (var.count == 0)
                    var.resource.release();

                var.counter.release();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
