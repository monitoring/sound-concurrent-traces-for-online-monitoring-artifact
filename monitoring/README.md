---
Monitoring Expriment
---


### How to run

Navigate to either directory to execute the program: 
- **prod-cons-correct**
- **prod-cons-faulty**

To execute the program, compile first using:

```
make refcompile
```

Then execute:

```
make refrun
```

To monitor with JavaMOP:

```
make sample N=1000  TOOL=jmop   SPEC=../specs/producer-consumer/prodcon.mop
```

JavaMOP relies on AspectJ and RV-Monitor to instrument and generate the runtime monitor. They are all found in the *tools* folder.

To clean JavaMOP generated files:

```
make jmopclean
```

Make sure to remove the compiled program when done using:

```
make clean
```

### Dependencies

To run JavaMOP you need to have Java version "1.8" 
