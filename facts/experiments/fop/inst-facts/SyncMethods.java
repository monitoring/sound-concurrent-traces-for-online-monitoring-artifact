import x.y.transformers.StaticInvocation;
import x.y.transformers.dynamiccontext.DynamicValue;
import x.y.transformers.dynamiccontext.MethodDynamicContext;
import x.y.transformers.staticcontext.Method;
import x.c.facts.events.EventType;
import x.c.facts.y.transformers.FastTransformer;

import org.objectweb.asm.Opcodes;

public class SyncMethods extends FastTransformer {

    final String owners = "owners";
    final String methods = "methods";

    @Override
    public void onMethodEnter(Method m, MethodDynamicContext dc) {

        if (((m.methodNode.access) & Opcodes.ACC_SYNCHRONIZED) != Opcodes.ACC_SYNCHRONIZED)
            return;

        DynamicValue threadId = dc.getThreadId(m);

        StaticInvocation inv = new StaticInvocation(observer.classPath, observer.methodName);
        //name, thread, resource, value
        inv.addParameter(1);
        inv.addParameter(threadId);

        if ((m.methodNode.access & Opcodes.ACC_STATIC) != 0)
            inv.addParameter(m.className);
        else
            inv.addParameter(dc.getThis(m));

        invoke(inv);
    }

    @Override
    public void onMethodExit(Method m, MethodDynamicContext dc) {

        if (((m.methodNode.access) & Opcodes.ACC_SYNCHRONIZED) != Opcodes.ACC_SYNCHRONIZED)
            return;

        DynamicValue threadId = dc.getThreadId(m);

        StaticInvocation inv = new StaticInvocation(observer.classPath, observer.methodName);
        //name, thread, resource, value
        inv.addParameter(2);
        inv.addParameter(threadId);
        if ((m.methodNode.access & Opcodes.ACC_STATIC) != 0)
            inv.addParameter(m.className);
        else
            inv.addParameter(dc.getThis(m));
        invoke(inv);
    }

}
