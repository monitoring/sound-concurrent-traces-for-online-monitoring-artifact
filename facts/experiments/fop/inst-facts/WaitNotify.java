import x.y.transformers.StaticInvocation;
import x.y.transformers.dynamiccontext.DynamicValue;
import x.y.transformers.dynamiccontext.MethodCallDynamicContext;
import x.y.transformers.staticcontext.MethodCall;
import x.c.facts.events.EventType;
import x.c.facts.y.transformers.FastTransformer;
import org.objectweb.asm.Opcodes;

public class WaitNotify extends FastTransformer {


    @Override
    public void beforeMethodCall(MethodCall mc, MethodCallDynamicContext dc) {

        if (mc.methodName.equals("notify") || mc.methodName.equals("notifyAll")) {

            if (mc.ins.basicBlock.method.name.contains("removeNode"))
                return;
            // Before await unlock
            DynamicValue threadId = dc.getThreadId(mc);

            StaticInvocation inv = new StaticInvocation(observer.classPath, observer.methodName);
            // name, thread, resource, value
            inv.addParameter(2);
            inv.addParameter(threadId);
            if (mc.ins.opcode == Opcodes.INVOKESTATIC) {
                inv.addParameter(mc.currentClassName);
            } else {
                DynamicValue methodReceiver = dc.getMethodReceiver(mc);
                inv.addParameter(methodReceiver);
            }
            invoke(inv);
        }

    }

    @Override
    public void afterMethodCall(MethodCall mc, MethodCallDynamicContext dc) {

        if (!mc.methodName.equals("wait"))
            return;

        // After await lock

        DynamicValue threadId = dc.getThreadId(mc);

        StaticInvocation inv = new StaticInvocation(observer.classPath, observer.methodName);
        // name, thread, resource, value
        inv.addParameter(1);
        inv.addParameter(threadId);
        if (mc.ins.opcode == Opcodes.INVOKESTATIC) {
            inv.addParameter(mc.currentClassName);
        } else {
            DynamicValue methodReceiver = dc.getMethodReceiver(mc);
            inv.addParameter(methodReceiver);
        }

        invoke(inv);

    }
}


