import x.y.transformers.StaticInvocation;
import x.y.transformers.dynamiccontext.DynamicValue;
import x.y.transformers.dynamiccontext.MethodCallDynamicContext;
import x.y.transformers.dynamiccontext.MethodDynamicContext;
import x.y.transformers.staticcontext.Method;
import x.y.transformers.staticcontext.MethodCall;
import x.c.facts.y.transformers.FastTransformer;

import java.util.Objects;

public class ActorSync extends FastTransformer {
    final String syncprovider = "syncprovider";
    final String acquiremethod = "acquiremethod";
    final String releasemethod = "releasemethod";


    public void afterMethodCall(MethodCall mc, MethodCallDynamicContext dc) {
        if (mc.methodOwner
                .contains("edu/rice/habanero/benchmarks/uct/UctAkkaActorBenchmark$NodeActor$")
                && mc.methodName.contains("createNodeActor")) {
            DynamicValue threadId = dc.getThreadId(mc);
            DynamicValue resource = dc.getMethodArgs(mc, 2);
            StaticInvocation inv = new StaticInvocation(observer.classPath, observer.methodName);

            inv.addParameter(2);
            inv.addParameter(threadId);
            inv.addBoxedParameter(resource);
            this.invoke(inv);
        }

    }

    public void onMethodEnter(Method m, MethodDynamicContext dc) {
        if (m.className.contains("edu/rice/habanero/benchmarks/uct/UctAkkaActorBenchmark$NodeActor")
                && m.name.contains("process")) {
            DynamicValue threadId = dc.getThreadId(m);
            DynamicValue resource = dc.getInstanceField(m, "myRoot");
            StaticInvocation inv = new StaticInvocation(observer.classPath, observer.methodName);

            inv.addParameter(1);
            inv.addParameter(threadId);
            inv.addBoxedParameter(resource);
            this.invoke(inv);

            inv = new StaticInvocation(observer.classPath, observer.methodName);

            inv.addParameter(20);
            inv.addParameter(threadId);
            inv.addParameter("m");
            this.invoke(inv);
        }

    }
}
