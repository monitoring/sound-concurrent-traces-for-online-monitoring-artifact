import x.y.transformers.StaticInvocation;
import x.y.transformers.dynamiccontext.DynamicValue;
import x.y.transformers.dynamiccontext.MethodCallDynamicContext;
import x.y.transformers.staticcontext.MethodCall;
import x.c.facts.y.transformers.FastTransformer;

import java.util.Objects;

public class AtomicOps extends FastTransformer {


    public void afterMethodCall(MethodCall mc, MethodCallDynamicContext dc) {
        if (mc.ins.basicBlock.method.classContext.classNode.name.equals("AkkaActor")) {
            DynamicValue threadId;
            DynamicValue resource;
            if (mc.methodOwner.contains("java/util/concurrent/atomic/Atomic")
                    && mc.methodName.contains("get") && !mc.ins.methodName.contains("<init>")) {
                threadId = dc.getThreadId(mc);
                resource = dc.getMethodReceiver(mc);
                dc.getMethodResult(mc);

                StaticInvocation inv =
                        new StaticInvocation(observer.classPath, observer.methodName);
                inv.addParameter(3);
                inv.addParameter(threadId);
                inv.addParameter(resource);
                inv.addParameter(1);
                this.invoke(inv);
            }
        }
    }

    public void beforeMethodCall(MethodCall mc, MethodCallDynamicContext dc) {
        if (mc.ins.basicBlock.method.classContext.classNode.name.equals("Actor")) {
            DynamicValue threadId;
            DynamicValue resource;
            StaticInvocation inv;
            if (mc.methodOwner.contains("java/util/concurrent/atomic/Atomic")
                    && mc.methodName.contains("set") && !mc.ins.methodName.contains("<init>")) {
                threadId = dc.getThreadId(mc);
                resource = dc.getMethodReceiver(mc);

                inv = new StaticInvocation(observer.classPath, observer.methodName);
                inv.addParameter(4);
                inv.addParameter(threadId);
                inv.addParameter(resource);
                inv.addParameter(1);
                this.invoke(inv);
            }
        }
    }
}
