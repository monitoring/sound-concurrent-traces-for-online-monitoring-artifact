package bakery.nthreads;


public class Process implements Runnable {

    private SharedVarN var;
    private int nRuns = 0;
    private int id = 0;
    private int numberOfThreads = 0;

    public Process(SharedVarN var, int nRuns, int id, int numberOfThreads) {
        this.var = var;
        this.nRuns = nRuns;
        this.id = id;
        this.numberOfThreads = numberOfThreads;

    }

    @Override
    public void run() {
        for (int i = 0; i < nRuns; i++) {
            lock();
            inc();
            unlock();
        }
    }

    private void inc() {
        var.ns++;
    }

    private void unlock() {
        var.wticket(id, 0, id);
    }

    private void lock() {

        var.wchoosing(id, true, id);

        // Find the max value and add 1 to get the next available ticket.

        var.wticket(id, var.findMax(id) + 1, id);

        var.wchoosing(id, false, id);

        for (int j = 0; j < numberOfThreads; j++) {

            // If the thread j is the current thread go the next thread.
            if (j == id)
                continue;

            // Wait if thread j is choosing right now.
            while (var.rchoosing(j, id)) {
                /* nothing */
            }

            while (var.rticket(j, id) != 0 && (var.rticket(id, id) > var.rticket(j, id)
                    || (var.rticket(id, id) == var.rticket(j, id) && id > j))) {
                /* nothing */
            }

        } // for
    }

}
