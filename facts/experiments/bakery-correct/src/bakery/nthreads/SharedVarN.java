package bakery.nthreads;


public class SharedVarN {

    public volatile int[] ticket;
    public volatile boolean[] choosing;

    public volatile int ns = 0;

    public Object lock = new Object();


    public SharedVarN(int numberOfThreads) {
        ticket = new int[numberOfThreads];
        choosing = new boolean[numberOfThreads];
    }

    public int rticket(int i, int id) {
        int x = ticket[i];

        return x;
    }

    public void wticket(int i, int v, int id) {

        // System.out.println( ticket);
        ticket[i] = v;
        // System.out.println("woops" + ticket);
    }

    public int findMax(int id) {

        int m = rticket(0, id);
        for (int i = 1; i < ticket.length; i++) {
            if (rticket(i, id) > m)
                m = rticket(i, id);
        }
        return m;
    }

    public void wchoosing(int i, boolean v, int id) {

        choosing[i] = v;
    }


    public boolean rchoosing(int i, int id) {
        boolean x = choosing[i];
        return x;
    }
}
