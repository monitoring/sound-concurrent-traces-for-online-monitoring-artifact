import x.y.transformers.StaticInvocation;
import x.y.transformers.dynamiccontext.DynamicValue;
import x.y.transformers.dynamiccontext.InstructionDynamicContext;
import x.y.transformers.dynamiccontext.MethodDynamicContext;
import x.y.transformers.staticcontext.Instruction;
import x.y.transformers.staticcontext.Method;
import x.c.facts.y.transformers.FastTransformer;
import x.c.facts.events.EventType;
import org.objectweb.asm.Opcodes;

public class ArrayReadAndWrite extends FastTransformer {


    final String SCOPE = "in-classes";
    DynamicValue threadId;

    @Override
    public void onMethodEnter(Method m, MethodDynamicContext dc) {
        if (filterAccess(SCOPE, m.className, true)) return;
        threadId = dc.getThreadId(m);
    }


    @Override
    public void beforeInstruction(Instruction ins, InstructionDynamicContext dc) {

        if (filterAccess(SCOPE, ins.basicBlock.method.className, true)) return;

        if (ins.opcode == Opcodes.BASTORE || ins.opcode == Opcodes.IASTORE) {

            int stackSize = ins.getBasicValueFrame().getStackSize();
            DynamicValue resourceObject = dc.getStackValue(ins, stackSize - 3);
            resourceObject.box();
            DynamicValue resourceName = dc.getStackValue(ins, stackSize - 2);
            resourceName.box();

            DynamicValue value = dc.getArrayWriteValue(ins);

            StaticInvocation inv = new StaticInvocation(observer.classPath, observer.methodName);
            inv.addParameter(4);
            inv.addParameter(threadId);
            inv.addParameter(resourceObject);
            inv.addParameter(resourceName);
            inv.addBoxedParameter(value);
            invoke(inv);

        }
    }

    @Override
    public void afterInstruction(Instruction ins, InstructionDynamicContext dc) {

        if (filterAccess(SCOPE, ins.basicBlock.method.className, true)) return;

        if (ins.opcode == Opcodes.BALOAD || ins.opcode == Opcodes.IALOAD) {

            int stackSize = ins.getBasicValueFrame().getStackSize();
            DynamicValue resourceObject = dc.getStackValue(ins, stackSize - 2);
            resourceObject.box();
            DynamicValue resourceName = dc.getStackValue(ins, stackSize - 1);
            resourceName.box();

            DynamicValue value = dc.getArrayReadValue(ins);

            StaticInvocation inv = new StaticInvocation(observer.classPath, observer.methodName);
            inv.addParameter(3);
            inv.addParameter(threadId);
            inv.addParameter(resourceObject);
            inv.addParameter(resourceName);
            inv.addBoxedParameter(value);
            invoke(inv);

        }
    }

}
