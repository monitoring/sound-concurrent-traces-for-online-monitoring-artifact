import x.y.transformers.StaticInvocation;
import x.y.transformers.Transformer;
import x.y.transformers.dynamiccontext.DynamicValue;
import x.y.transformers.dynamiccontext.MethodCallDynamicContext;
import x.y.transformers.dynamiccontext.InstructionDynamicContext;
import x.y.transformers.dynamiccontext.LocalArray;
import x.y.transformers.dynamiccontext.MethodDynamicContext;
import x.y.transformers.staticcontext.Method;
import x.y.transformers.staticcontext.MethodCall;
import x.c.facts.y.transformers.FastTransformer;
import org.objectweb.asm.Opcodes;
import x.y.transformers.staticcontext.Instruction;
import org.objectweb.asm.tree.InsnNode;
import org.objectweb.asm.tree.MethodInsnNode;
import org.objectweb.asm.tree.VarInsnNode;

import java.util.Arrays;
import java.util.List;

public class MethodCallsFA extends FastTransformer {

    final String OWNERS = "owners";
    final String METHODS = "methods";

    int i = 0;

    int lvUpdate;

    @Override
    public void beforeMethodCall(MethodCall mc, MethodCallDynamicContext dc) {

        if (filterAccess(OWNERS, METHODS, mc.methodOwner, mc.methodName, true))
            return;

        lvUpdate = mc.ins.basicBlock.method.methodNode.maxLocals;
        mc.ins.basicBlock.method.methodNode
                .visitMaxs(mc.ins.basicBlock.method.methodNode.maxStack + 1, lvUpdate + 1);

        entermonitor(mc, lvUpdate);

        DynamicValue methodReceiver = dc.getMethodReceiver(mc);
        DynamicValue threadId = dc.getThreadId(mc);

        StaticInvocation inv = new StaticInvocation(observer.classPath2, observer.methodName);
        if (mc.methodName.contains("read")) { // add
            inv.addParameter(22);
            inv.addParameter(threadId);
            String g = "BC @" + mc.currentClassName + "." + mc.ins.methodName + "." + "ln:"
                    + mc.ins.linenumber;
            inv.addParameter(g);
        } else { // poll
            inv.addParameter(20);
            inv.addParameter(threadId);
            String g = "BP @" + mc.currentClassName + "." + mc.ins.methodName + "." + "ln:"
                    + mc.ins.linenumber;
            inv.addParameter(g);
        }

        inv.addParameter(methodReceiver);

        invoke(inv);

    }

    private void entermonitor(MethodCall mc, int lvn) {
        insert(new MethodInsnNode(Opcodes.INVOKESTATIC, "x/c/facts/Synchronizer", "getLock",
                "()Ljava/lang/Object;", false));

        insert(new InsnNode(Opcodes.DUP));
        insert(new VarInsnNode(Opcodes.ASTORE, lvn));
        insert(new InsnNode(Opcodes.MONITORENTER));
    }


    @Override
    public void afterMethodCall(MethodCall mc, MethodCallDynamicContext dc) {

        if (filterAccess(OWNERS, METHODS, mc.methodOwner, mc.methodName, true))
            return;

        DynamicValue methodReceiver = dc.getMethodReceiver(mc);
        DynamicValue threadId = dc.getThreadId(mc);

        StaticInvocation inv = new StaticInvocation(observer.classPath2, observer.methodName);
        if (mc.methodName.contains("read")) { // add
            inv.addParameter(23);
            inv.addParameter(threadId);
            String g = "AC @" + mc.currentClassName + "." + mc.ins.methodName + "." + "ln:"
                    + mc.ins.linenumber;
            inv.addParameter(g);
        } else { // poll
            inv.addParameter(21);
            inv.addParameter(threadId);
            String g = "AP @" + mc.currentClassName + "." + mc.ins.methodName + "." + "ln:"
                    + mc.ins.linenumber;
            inv.addParameter(g);
        }

        inv.addParameter(methodReceiver);

        invoke(inv);

        insert(new VarInsnNode(Opcodes.ALOAD, lvUpdate));
        insert(new InsnNode(Opcodes.MONITOREXIT));

    }

}
