import x.y.transformers.StaticInvocation;
import x.y.transformers.dynamiccontext.DynamicValue;
import x.y.transformers.dynamiccontext.MethodCallDynamicContext;
import x.y.transformers.staticcontext.MethodCall;
import x.c.facts.y.transformers.FastTransformer;
import x.c.facts.events.EventType;

public class AcquireRelease extends FastTransformer {

    final String syncprovider = "syncprovider";
    final String acquiremethod = "acquiremethod";
    final String releasemethod = "releasemethod";

    @Override
    public void afterMethodCall(MethodCall mc, MethodCallDynamicContext dc) {
        if (filterAccess(syncprovider, acquiremethod, mc.methodOwner, mc.methodName, true))
            return;

        DynamicValue threadId = dc.getThreadId(mc);
        DynamicValue resource = dc.getMethodReceiver(mc);

        StaticInvocation inv = new StaticInvocation(observer.classPath, observer.methodName);

        inv.addParameter(1);
        inv.addParameter(threadId);
        inv.addParameter(resource);

        invoke(inv);

    }

    @Override
    public void beforeMethodCall(MethodCall mc, MethodCallDynamicContext dc) {
        if (filterAccess(syncprovider, releasemethod, mc.methodOwner, mc.methodName, true))
            return;

        DynamicValue threadId = dc.getThreadId(mc);
        DynamicValue resource = dc.getMethodReceiver(mc);

        StaticInvocation inv = new StaticInvocation(observer.classPath, observer.methodName);

        inv.addParameter(2);
        inv.addParameter(threadId);
        inv.addParameter(resource);
        invoke(inv);

    }
}
