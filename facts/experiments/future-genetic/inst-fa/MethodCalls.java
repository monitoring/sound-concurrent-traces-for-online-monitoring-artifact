import x.y.transformers.StaticInvocation;
import x.y.transformers.Transformer;
import x.y.transformers.dynamiccontext.DynamicValue;
import x.y.transformers.dynamiccontext.LocalArray;
import x.y.transformers.staticcontext.Method;
import x.y.transformers.staticcontext.MethodCall;
import org.objectweb.asm.Opcodes;
import x.y.transformers.dynamiccontext.InstructionDynamicContext;
import x.y.transformers.staticcontext.Instruction;
import x.y.transformers.dynamiccontext.MethodCallDynamicContext;
import x.y.transformers.dynamiccontext.MethodDynamicContext;
import x.c.facts.y.transformers.FastTransformer;
import org.objectweb.asm.tree.InsnNode;
import org.objectweb.asm.tree.MethodInsnNode;
import org.objectweb.asm.tree.VarInsnNode;

public class MethodCalls extends FastTransformer {

    final String methods = "methods";
    int lvUpdate;

    @Override
    public void onMethodEnter(Method m, MethodDynamicContext dc) {
        if (filterAccess(methods, m.name, false))
            return;
        lvUpdate = m.methodNode.maxLocals;
        m.methodNode
                .visitMaxs(m.methodNode.maxStack + 1, lvUpdate + 1);

        entermonitor(m, lvUpdate);

        DynamicValue threadId = dc.getThreadId(m);
        // DynamicValue methodReceiver = dc.getMethodReceiver(mc);
        StaticInvocation inv = new StaticInvocation(observer.classPath2, observer.methodName);

        inv.addParameter(20);
        inv.addParameter(threadId);
        inv.addParameter("2");

        invoke(inv);

    }

    private void entermonitor(Method m, int lvn) {
        insert(new MethodInsnNode(Opcodes.INVOKESTATIC, "x/c/facts/Synchronizer", "getLock",
                "()Ljava/lang/Object;", false));

        insert(new InsnNode(Opcodes.DUP));

        insert(new VarInsnNode(Opcodes.ASTORE, lvn));
        insert(new InsnNode(Opcodes.MONITORENTER));
    }

    @Override
    public void onMethodExit(Method m, MethodDynamicContext dc) {
        if (filterAccess(methods, m.name, false))
            return;

        insert(new VarInsnNode(Opcodes.ALOAD, lvUpdate));
        insert(new InsnNode(Opcodes.MONITOREXIT));

    }

}
