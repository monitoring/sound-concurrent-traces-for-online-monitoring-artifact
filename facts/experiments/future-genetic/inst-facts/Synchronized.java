import x.y.transformers.StaticInvocation;
import x.y.transformers.dynamiccontext.DynamicValue;
import x.y.transformers.dynamiccontext.InstructionDynamicContext;
import x.y.transformers.dynamiccontext.MethodDynamicContext;
import x.y.transformers.staticcontext.Instruction;
import x.y.transformers.staticcontext.Method;
import x.c.facts.y.transformers.FastTransformer;

import java.util.Objects;

public class Synchronized extends FastTransformer {

    public void onMethodEnter(Method m, MethodDynamicContext dc) {
        if ((m.methodNode.access & 32) == 32) {
            DynamicValue threadId = dc.getThreadId(m);
            StaticInvocation inv = new StaticInvocation(observer.classPath, observer.methodName);
            inv.addParameter(1);
            inv.addParameter(threadId);
            if ((m.methodNode.access & 8) != 0) {
                inv.addParameter(m.className);
            } else {
                inv.addParameter(dc.getThis(m));
            }

            this.invoke(inv);
        }
    }

    public void onMethodExit(Method m, MethodDynamicContext dc) {
        if ((m.methodNode.access & 32) == 32) {
            DynamicValue threadId = dc.getThreadId(m);
            StaticInvocation inv = new StaticInvocation(observer.classPath, observer.methodName);
            inv.addParameter(2);
            inv.addParameter(threadId);
            if ((m.methodNode.access & 8) != 0) {
                inv.addParameter(m.className);
            } else {
                inv.addParameter(dc.getThis(m));
            }

            this.invoke(inv);
        }
    }

    public void beforeInstruction(Instruction ins, InstructionDynamicContext dc) {
        if (!ins.methodName.toLowerCase().contains("init")) {
            if (ins.opcode == 195) {
                DynamicValue threadId = dc.getThreadId(ins);
                StaticInvocation inv = new StaticInvocation(observer.classPath, observer.methodName);
                inv.addParameter(2);
                inv.addParameter(threadId);
                inv.addParameter(dc.getThis(ins));
                this.invoke(inv);
            }

        }
    }

    public void afterInstruction(Instruction ins, InstructionDynamicContext dc) {
        if (!ins.methodName.toLowerCase().contains("init")) {
            if (ins.opcode == 194) {
                DynamicValue threadId = dc.getThreadId(ins);
                StaticInvocation inv = new StaticInvocation(observer.classPath, observer.methodName);
                inv.addParameter(1);
                inv.addParameter(threadId);
                inv.addParameter(dc.getThis(ins));
                this.invoke(inv);
            }

        }
    }
}
