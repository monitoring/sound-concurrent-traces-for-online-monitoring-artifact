import x.y.transformers.StaticInvocation;
import x.y.transformers.dynamiccontext.DynamicValue;
import x.y.transformers.dynamiccontext.MethodCallDynamicContext;
import x.y.transformers.dynamiccontext.MethodDynamicContext;
import x.y.transformers.staticcontext.Method;
import x.y.transformers.staticcontext.MethodCall;
import x.c.facts.y.transformers.FastTransformer;

import java.util.Objects;

public class MethodCalls extends FastTransformer {


    final String methods = "methods";


    @Override
    public void onMethodEnter(Method m, MethodDynamicContext dc) {

        if (filterAccess(methods, m.name, false))
            return;

        DynamicValue threadId = dc.getThreadId(m);
        StaticInvocation inv = new StaticInvocation(observer.classPath, observer.methodName);

        inv.addParameter(20);
        inv.addParameter(threadId);
        inv.addParameter("m");
        invoke(inv);
    }

}
