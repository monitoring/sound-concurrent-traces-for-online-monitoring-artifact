package semaphore;

import java.util.concurrent.Semaphore;

public class SharedVar {
    public Semaphore service;
    public Semaphore resource;
    public Semaphore counter;
    public int count = 0;
    private int[] arr;

    public SharedVar(int size) {
        arr = new int[size];
        service = new Semaphore(1);
        resource = new Semaphore(1);
        counter = new Semaphore(1);
    }

    public int readArr() {
        int sum = 0;
        for (int i = 0; i < arr.length; i++)
            sum += arr[i];

        return sum;
    }

    public void writeArr(int x) {
        for (int i = 0; i < arr.length; i++)
            arr[i] = x;
    }
}
