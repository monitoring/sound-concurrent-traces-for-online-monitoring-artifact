package semaphore;

import java.util.Random;

public class Writer implements Runnable {
    Random r = new Random();
    private SharedVar var;
    private int nWrites;

    public Writer(SharedVar var, int nWrites) {
        this.var = var;
        this.nWrites = nWrites;
    }

    @Override
    public void run() {
        try {
            for (int i = 0; i < nWrites; i++) {
                var.service.acquire();
                var.resource.acquire();
                var.service.release();

                var.writeArr(r.nextInt());

                var.resource.release();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
