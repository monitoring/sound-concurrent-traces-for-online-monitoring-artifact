import x.y.transformers.StaticInvocation;
import x.y.transformers.dynamiccontext.MethodDynamicContext;
import x.y.transformers.staticcontext.Method;
import x.c.facts.y.transformers.FastTransformer;

public class OnExit extends FastTransformer {

    final String owners = "owners";
    final String methods = "methods";

    @Override
    public void onMethodExit(Method m, MethodDynamicContext dc) {

        if (filterAccess(owners, methods, m.className, m.name, true))
            return;

        StaticInvocation inv = new StaticInvocation(observer.classPath, "finish");
        invoke(inv);

    }
}
