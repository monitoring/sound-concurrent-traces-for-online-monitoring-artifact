import x.y.transformers.dynamiccontext.DynamicValue;
import x.y.transformers.dynamiccontext.MethodCallDynamicContext;
import x.y.transformers.staticcontext.MethodCall;
import x.y.transformers.staticcontext.Instruction;
import x.c.facts.y.transformers.FastTransformer;
import x.c.facts.events.EventType;
import x.y.transformers.StaticInvocation;
import x.y.transformers.dynamiccontext.MethodDynamicContext;
import x.y.transformers.staticcontext.Method;

public class AAA extends FastTransformer {

    final String owners = "owners";
    final String methods = "methods";

    int i = 0;

    @Override
    public void afterMethodCall(MethodCall mc, MethodCallDynamicContext dc) {

        if (filterAccess(owners, methods, mc.methodOwner, mc.methodName, false))
            return;

        DynamicValue threadId = dc.getThreadId(mc);

        StaticInvocation inv =
                new StaticInvocation(observer.classPath3, observer.methodName);

        inv.addParameter(20);
        inv.addParameter(threadId);
        inv.addParameter("m");

        invoke(inv);

    }

}
