import x.y.transformers.StaticInvocation;
import x.y.transformers.dynamiccontext.DynamicValue;
import x.y.transformers.dynamiccontext.FieldSetDynamicContext;
import x.y.transformers.dynamiccontext.LocalArray;
import x.y.transformers.staticcontext.FieldAccess;
import x.c.facts.y.transformers.FastTransformer;


public class CriticalSection extends FastTransformer {


    final String OWNERS = "owners";
    final String FIELDS = "fields";
    final String SCOPE = "in-classes";

    @Override
    public void beforeSetField(FieldAccess fa, FieldSetDynamicContext dc) {

        if (filterAccess(OWNERS, FIELDS, fa.fieldOwner, fa.fieldName, true))
            return;
        if (filterAccess(SCOPE, fa.ins.className, true))
            return;

        DynamicValue threadId = dc.getThreadId(fa);
        StaticInvocation inv = new StaticInvocation(observer.classPath, observer.methodName);

        LocalArray laKeys = dc.createLocalArray(fa.ins.basicBlock.method, String.class);
        dc.addToLocalArray(laKeys, "resource");

        LocalArray laValues = dc.createLocalArray(fa.ins.basicBlock.method, Object.class);
        dc.addToLocalArray(laValues, dc.getFieldOwnerInstance(fa));

        // name, thread
        inv.addParameter(20);
        inv.addParameter(threadId);
        inv.addParameter("C @" + fa.currentClassName + "." + fa.ins.methodName + "." + "ln:"
                + fa.ins.linenumber);
        inv.addParameter(laKeys);
        inv.addParameter(laValues);

        invoke(inv);

    }
}
