package bakery.nthreads;


public class MainN {


    public static void main(String[] args) {

        int run = Integer.parseInt(args[0]);
        int nThreads = Integer.parseInt(args[1]);

        SharedVarN var = new SharedVarN(nThreads);

        Runnable[] runs = new Runnable[nThreads];

        for (int i = 0; i < nThreads; i++) {
            runs[i] = new Process(var, run, i, nThreads);
        }

        Harness bench = new Harness(nThreads);
        bench.start(runs);
        bench.end();

        System.out.println(var.ns);

    }

}

