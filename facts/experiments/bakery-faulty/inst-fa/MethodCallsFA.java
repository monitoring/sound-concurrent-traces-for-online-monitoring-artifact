import x.y.transformers.StaticInvocation;
import x.y.transformers.dynamiccontext.MethodCallDynamicContext;
import x.y.transformers.dynamiccontext.MethodDynamicContext;
import x.c.facts.y.transformers.FastTransformer;
import x.y.transformers.dynamiccontext.InstructionDynamicContext;
import x.y.transformers.staticcontext.Instruction;
import x.y.transformers.Transformer;
import x.y.transformers.dynamiccontext.DynamicValue;
import x.y.transformers.dynamiccontext.LocalArray;
import x.y.transformers.staticcontext.Method;
import x.y.transformers.staticcontext.MethodCall;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.InsnNode;
import org.objectweb.asm.tree.MethodInsnNode;
import org.objectweb.asm.tree.VarInsnNode;

import java.util.Arrays;
import java.util.List;

public class MethodCallsFA extends FastTransformer {

    final String owners = "owners";
    final String methods = "methods";


    int i = 0;


    int lvUpdate;

    @Override
    public void beforeMethodCall(MethodCall mc, MethodCallDynamicContext dc) {

        if (filterAccess(owners, methods, mc.methodOwner, mc.methodName, false))
            return;

        lvUpdate = mc.ins.basicBlock.method.methodNode.maxLocals;
        mc.ins.basicBlock.method.methodNode
                .visitMaxs(mc.ins.basicBlock.method.methodNode.maxStack + 1, lvUpdate + 1);

        entermonitor(mc, lvUpdate);

        DynamicValue threadId = dc.getThreadId(mc);
        StaticInvocation inv = new StaticInvocation(observer.classPath2, observer.methodName);

        inv.addParameter(20);
        inv.addParameter(threadId);
        inv.addParameter("2");

        invoke(inv);

    }

    private void entermonitor(MethodCall mc, int lvn) {
        insert(new MethodInsnNode(Opcodes.INVOKESTATIC, "x/c/facts/Synchronizer", "getLock",
                "()Ljava/lang/Object;", false));

        insert(new InsnNode(Opcodes.DUP));
        insert(new VarInsnNode(Opcodes.ASTORE, lvn));
        insert(new InsnNode(Opcodes.MONITORENTER));
    }


    @Override
    public void afterMethodCall(MethodCall mc, MethodCallDynamicContext dc) {

        if (filterAccess(owners, methods, mc.methodOwner, mc.methodName, false))
            return;

        insert(new VarInsnNode(Opcodes.ALOAD, lvUpdate));
        insert(new InsnNode(Opcodes.MONITOREXIT));

    }

}
