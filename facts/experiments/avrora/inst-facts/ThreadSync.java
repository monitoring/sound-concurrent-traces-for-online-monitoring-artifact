import x.y.transformers.StaticInvocation;
import x.y.transformers.dynamiccontext.DynamicValue;
import x.y.transformers.dynamiccontext.MethodCallDynamicContext;
import x.y.transformers.staticcontext.MethodCall;
import x.c.facts.y.transformers.FastTransformer;
import x.c.facts.events.EventType;
import org.objectweb.asm.Opcodes;

public class ThreadSync extends FastTransformer {

    final String THREAD = "thread";
    final String fork = "fork";
    final String join = "join";

    @Override
    public void afterMethodCall(MethodCall mc, MethodCallDynamicContext dc) {
        if (filterAccess(THREAD, join, mc.methodOwner, mc.methodName, false))
            return;

        DynamicValue threadId = dc.getThreadId(mc);

        StaticInvocation inv = new StaticInvocation(observer.classPath, observer.methodName);

        inv.addParameter(8);
        inv.addParameter(threadId);
        if (mc.ins.opcode == Opcodes.INVOKESTATIC) {
            inv.addParameter(mc.currentClassName);
        } else {
            DynamicValue methodReceiver = dc.getMethodReceiver(mc);
            inv.addParameter(methodReceiver);
        }

        invoke(inv);

    }

    @Override
    public void beforeMethodCall(MethodCall mc, MethodCallDynamicContext dc) {
        if (filterAccess(THREAD, fork, mc.methodOwner, mc.methodName, false))
            return;

        DynamicValue threadId = dc.getThreadId(mc);

        StaticInvocation inv = new StaticInvocation(observer.classPath, observer.methodName);

        inv.addParameter(7);
        inv.addParameter(threadId);
        if (mc.ins.opcode == Opcodes.INVOKESTATIC) {
            inv.addParameter(mc.currentClassName);
        } else {
            DynamicValue methodReceiver = dc.getMethodReceiver(mc);
            inv.addParameter(methodReceiver);
        }
        invoke(inv);

    }
}
