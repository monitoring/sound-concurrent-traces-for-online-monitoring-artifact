import x.y.transformers.StaticInvocation;
import x.y.transformers.dynamiccontext.DynamicValue;
import x.y.transformers.dynamiccontext.InstructionDynamicContext;
import x.y.transformers.staticcontext.Instruction;
import x.c.facts.events.EventType;
import org.objectweb.asm.Opcodes;
import x.c.facts.y.transformers.FastTransformer;
import x.y.transformers.dynamiccontext.MethodDynamicContext;
import x.y.transformers.staticcontext.Method;

import java.util.stream.IntStream;

public class SynchronizedBlock extends FastTransformer {

    final String owners = "owners";

    @Override
    public void beforeInstruction(Instruction ins, InstructionDynamicContext dc) {

        // Acquired a lock
        if (ins.opcode == Opcodes.MONITOREXIT) {

            if (ins.methodName.toLowerCase().contains("init"))
                return;

            if (ins.getBasicValueFrame() == null) return;
            int stackSize = ins.getBasicValueFrame().getStackSize();
            DynamicValue resource = dc.getStackValue(ins, stackSize - 1);

            DynamicValue threadId = dc.getThreadId(ins);

            StaticInvocation inv = new StaticInvocation(observer.classPath, observer.methodName);
            // name, thread, resource, value
            inv.addParameter(2);
            inv.addParameter(threadId);
            inv.addParameter(resource);
            invoke(inv);

        }
    }

    @Override
    public void afterInstruction(Instruction ins, InstructionDynamicContext dc) {

        // Released a lock
        if (ins.opcode == Opcodes.MONITORENTER) {

            if (ins.methodName.toLowerCase().contains("init"))
                return;

            if (ins.getBasicValueFrame() == null) return;
            int stackSize = ins.getBasicValueFrame().getStackSize();
            DynamicValue resource = dc.getStackValue(ins, stackSize - 1);

            DynamicValue threadId = dc.getThreadId(ins);
            StaticInvocation inv = new StaticInvocation(observer.classPath, observer.methodName);
            // name, thread, resource, value
            inv.addParameter(1);
            inv.addParameter(threadId);
            inv.addParameter(resource);
            invoke(inv);

        }
    }

}
