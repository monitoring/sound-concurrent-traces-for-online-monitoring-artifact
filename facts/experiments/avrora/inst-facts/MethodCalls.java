import x.y.transformers.StaticInvocation;
import x.y.transformers.Transformer;
import x.y.transformers.dynamiccontext.DynamicValue;
import x.y.transformers.dynamiccontext.LocalArray;
import x.y.transformers.dynamiccontext.MethodCallDynamicContext;
import x.y.transformers.dynamiccontext.MethodDynamicContext;
import x.y.transformers.staticcontext.Method;
import x.y.transformers.staticcontext.MethodCall;
import x.c.facts.y.transformers.FastTransformer;
import org.objectweb.asm.Opcodes;
import x.y.transformers.dynamiccontext.InstructionDynamicContext;
import x.y.transformers.staticcontext.Instruction;

import java.util.Arrays;
import java.util.List;

public class MethodCalls extends FastTransformer {

    final String owners = "owners";
    final String methods = "methods";

    int i = 0;

    @Override
    public void beforeMethodCall(MethodCall mc, MethodCallDynamicContext dc) {

        if (mc.ins.methodName.contains("init"))
            return;

        if (filterAccess(owners, mc.methodOwner, false))
            return;

        if (filterAccess(methods, mc.methodName, false))
            return;

        DynamicValue threadId = dc.getThreadId(mc);
        StaticInvocation inv = new StaticInvocation(observer.classPath, observer.methodName);

        inv.addParameter(20);
        inv.addParameter(threadId);
        inv.addParameter("2");

        invoke(inv);

    }

}
