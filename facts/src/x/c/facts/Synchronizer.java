package x.c.facts;

public class Synchronizer {

    public static Object myLock = new Object();

    public static void test() {

        synchronized (Synchronizer.getLock()) {
            foo();
        }
    }

    public static Object getLock() {
        return myLock;
    }

    private static void foo() {
    }

}
