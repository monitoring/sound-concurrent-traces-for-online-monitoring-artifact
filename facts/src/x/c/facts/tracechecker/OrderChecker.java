package x.c.facts.tracechecker;

import x.c.facts.events.Event;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

public class OrderChecker {

    private final List<Event> trace;
    public HashSet<AbstractMap.SimpleEntry<String, String>> dependencyPairs;
    public List<AbstractMap.SimpleEntry<Integer, Integer>> unOrderedPairs = new ArrayList<>();
    public long unOrderedPairsCount = 0;
    public long orderedPairsCount = 0;
    public long orderedPairsInThreadsCount = 0;
    public HashSet<AbstractMap.SimpleEntry<String, String>> unorderedEvents = new HashSet<>();
    public int processedEventsIndex = 0;


    public OrderChecker(HashSet<AbstractMap.SimpleEntry<String, String>> requiredOrderings, List<Event> trace) {
        dependencyPairs = new HashSet<>();
        HashSet<String> duplicatesChecker = new HashSet<>();

        if (requiredOrderings != null) {
            requiredOrderings.forEach(e -> {

                String key = e.getKey();
                String value = e.getValue();

                if (!duplicatesChecker.contains(key + value)) {
                    dependencyPairs.add(new AbstractMap.SimpleEntry<>(key, value));
                    duplicatesChecker.add(key + value);
                    duplicatesChecker.add(value + key);
                }

            });
        }
        this.trace = trace;

    }


    public boolean findMissingOrders() {

        if (dependencyPairs.isEmpty())
            return false;

        //Get trace size
        int traceSize = trace.size();
        List<Event> unprocessed = trace.subList(processedEventsIndex, traceSize);

        if (unprocessed.isEmpty()) {
            return false;
        }
        processedEventsIndex = traceSize;

        for (AbstractMap.SimpleEntry<String, String> ops : dependencyPairs) {

            Integer key = Integer.valueOf(ops.getKey());
            Integer value = Integer.valueOf(ops.getValue());

            List<Event> reEvents = unprocessed.stream().filter(t -> t.name >= 20)
                    .filter(e -> e.name == key || e.name == value).collect(Collectors.toList());

            for (int k = 0; k < reEvents.size() - 1; k++) {
                Event from = reEvents.get(k);
                Event to = reEvents.get(k + 1);
                if (from.thread.equals(to.thread)) {
                    orderedPairsInThreadsCount++;
                    continue;
                }

                if (from.name == (key) && to.name == (value) || (from.name == (value) && to.name == (key))) {

                    boolean fromhbto = from.happensBefore(to);
                    boolean tohbfrom = to.happensBefore(from);
//
                    if (!fromhbto && !tohbfrom) {
                        unOrderedPairsCount++;
                    } else {
                        orderedPairsCount++;
                    }
                }
            }
        }
        return true;
    }

    public boolean findParametricMissingOrders() {

        if (dependencyPairs.isEmpty())
            return false;

        //Get trace size
        int traceSize = trace.size();
        List<Event> unprocessed = trace.subList(processedEventsIndex, traceSize);
        if (unprocessed.isEmpty()) {
            return false;
        }
        processedEventsIndex = traceSize;

        for (AbstractMap.SimpleEntry<String, String> ops : dependencyPairs) {

            Integer key = Integer.valueOf(ops.getKey());
            Integer value = Integer.valueOf(ops.getValue());

            List<Event> reEvents = unprocessed.stream().filter(t -> t.name >= 20)
                    .filter(e -> e.name == key || e.name == value).collect(Collectors.toList());

            for (int k = 0; k < reEvents.size() - 1; k++) {
                Event from = reEvents.get(k);

                Event to = reEvents.get(k + 1);

                if (from.thread.equals(to.thread) || !from.hasCompatibleBinding(to)) {

                    continue;
                }

                if (from.name == (key) && to.name == (value) || (from.name == (value) && to.name == (key))) {

                    boolean fromhbto = from.happensBefore(to);
                    boolean tohbfrom = to.happensBefore(from);

                    if (!fromhbto && !tohbfrom) {

                        unOrderedPairsCount++;
                    } else {
                        orderedPairsCount++;
                    }

                }
            }

        }
        return true;
    }


    public String getUnorderedEvents() {

        return unorderedEvents.toString();

    }
}
