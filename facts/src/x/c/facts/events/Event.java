package x.c.facts.events;


import x.c.facts.vectorclock.ds.TreeClock;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

public class Event {

    public int index;
    public int name;
    public Long thread;
    public String address;
    public Object resource;
    public int hashedResource;
    public Object value;
    public TreeClock timeStamp;
    public Map<String, Object> variableBindings;
    private HashSet<Event> happensBefore;

    public Event(int name, long thread, Object resource, Object value) {
        this.name = name;
        this.thread = thread;
        this.resource = resource;
        this.value = value;

    }

    public Event(int name, long thread, int resource) {
        this.name = name;
        this.thread = thread;
        this.hashedResource = resource;
        this.resource = resource;

    }

    public Event(int name, long thread, Object resource) {
        this.name = name;
        this.thread = thread;

        this.resource = resource;

    }

    public Event(int name, long thread, String address, List<String> variableNames, List<Object> variableBindings) {
        this.name = name;
        this.thread = thread;
        this.address = address;

        if (variableNames == null || variableBindings == null || variableNames.size() != variableBindings.size()) {
            return;
        }
        this.variableBindings = new HashMap<>();
        IntStream.range(0, variableNames.size()).forEach(i -> this.variableBindings.put(variableNames.get(i), variableBindings.get(i)));

    }

    public Event(int name, long thread, Object resource, List<String> variableNames, List<Object> variableBindings) {
        this.name = name;
        this.thread = thread;
        this.resource = resource;

        if (variableNames == null || variableBindings == null || variableNames.size() != variableBindings.size()) {
            return;
        }
        this.variableBindings = new HashMap<>();
        IntStream.range(0, variableNames.size()).forEach(i -> this.variableBindings.put(variableNames.get(i), variableBindings.get(i)));

    }

    public Event(int name, long thread, String address, Object resource, Object value) {

        this.name = name;
        this.thread = thread;
        this.address = address;
        this.resource = resource;
        this.value = value;

    }

    public Long getThread() {
        return thread;

    }


    public boolean happensBefore(Event e) {

        return this.timeStamp.isLessThanOrEqual(e.timeStamp);
    }

    public boolean hasCompatibleBinding(Event to) {

        if (variableBindings == null || to.variableBindings == null || variableBindings.isEmpty() || to.variableBindings.isEmpty()) {
            return false;
        }

        boolean isCompatible = false;
        for (String key : variableBindings.keySet()) {
            if (to.variableBindings.containsKey(key)) {
                if (!variableBindings.get(key).equals(to.variableBindings.get(key))) {
                    return false;
                } else {
                    isCompatible = true;
                }
            }
        }

        return isCompatible;
    }


    public String toStringWithVC() {
        String lazyToString;
        final StringBuilder formatted = new StringBuilder();
        if (name == EventType.READ || name == EventType.WRITE) {
            formatted.append("t").append(thread).append(".").append(name).append(".").append(resource).append(".").append(value).append(".").append(timeStamp.timesToString());
        } else if (name == EventType.FORK || name == (EventType.JOIN)) {
            formatted.append("t").append(thread).append(".").append(name).append(".").append(resource).append(".").append(timeStamp.timesToString());
        } else if (variableBindings != null && !variableBindings.isEmpty()) {
            formatted.append("t").append(thread).append(".").append(name).append(".").append(timeStamp.timesToString());
        } else
            formatted.append("t").append(thread).append(".").append(name).append(".").append(timeStamp.timesToString());
        lazyToString = formatted.toString();
        return lazyToString;
    }

}
