package x.c.facts.events;

public class EventType {


    public static final int LOCK = 1;
    public static final int UNLOCK = 2;

    public static final int READ = 3;
    public static final int WRITE = 4;

    public static final int START = 5;
    public static final int END = 6;


    public static final int FORK = 7;
    public static final int JOIN = 8;

}
