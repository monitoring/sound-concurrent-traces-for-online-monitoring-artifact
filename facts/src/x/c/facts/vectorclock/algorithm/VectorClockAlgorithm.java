package x.c.facts.vectorclock.algorithm;

import x.c.facts.RunSpecification;
import x.c.facts.events.Event;
import x.c.facts.events.EventType;
import x.c.facts.vectorclock.ds.TreeClock;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;

public class VectorClockAlgorithm {


    public final HashMap<Long, Short> seenThreads;
    public final ConcurrentHashMap<Object, Event> lastUnlock;
    public final ConcurrentHashMap<Object, Event> lastFork;
    public final HashMap<Object, HashMap<Object, Event>> lastWriteX;
    private final Queue<Event> observation;
    private final List<Event> trace;
    private final HashMap<Long, TreeClock> lastInThread;
    public HashSet<String> threads = new HashSet<>();


    public long numberOfswEdges;

    public int eventIndex = 0;
    public long SAs = 0;

    public int dimensions = 0;
    public long numberOfEventsReceived;


    public VectorClockAlgorithm(Queue<Event> observation, List<Event> trace) {
        this.observation = observation;
        this.trace = trace;

        lastInThread = new HashMap<>();
        seenThreads = new HashMap<>();
        lastUnlock = new ConcurrentHashMap<>();
        lastFork = new ConcurrentHashMap<>();
        lastWriteX = new HashMap<>();
        dimensions = RunSpecification.numberOfThreads;

    }

    public synchronized boolean processEntry() {

        boolean entered = false;
        while (!observation.isEmpty()) {

            eventIndex++;
            Event e = null;
            e = observation.poll();
            if (e == null) {
                System.out.println(observation.size());
                continue;
            }
            numberOfEventsReceived++;
            e.index = eventIndex;

            boolean firstEventinThread = false;

            if (!seenThreads.containsKey(e.thread)) {

                short newtid = (short) (seenThreads.size());
                seenThreads.put(e.thread, newtid);

                try {
                    e.timeStamp = new TreeClock(newtid, 1, dimensions);
                } catch (Exception ex) {
                    System.out.println(seenThreads);
                    throw ex;
                }

                firstEventinThread = true;

            } else {
                e.timeStamp = new TreeClock(dimensions);
                e.timeStamp.deepCopy(lastInThread.get(e.thread));
            }

            switch (e.name) {
                case EventType.LOCK:

                    Event j = lastUnlock.get(e.hashedResource);
                    if (j != null && !e.thread.equals(j.thread)) {
                        e.timeStamp.join(j.timeStamp);
                        numberOfswEdges++;
                    }

                    update(e, firstEventinThread);
                    break;

                case EventType.WRITE:

                    Event lw = (lastWriteX.get(e.resource) == null) ? null : lastWriteX.get(e.resource).get(e.value);

                    if (lw == null || !e.value.equals(lw.value) || lw.happensBefore(e)) {
                        if (lastWriteX.get(e.resource) == null) {
                            lastWriteX.put(e.resource, new HashMap<>());
                        }
                        lastWriteX.get(e.resource).put(e.value, e);

                    } else {
                        if (lastWriteX.get(e.resource) == null) {
                            lastWriteX.put(e.resource, new HashMap<>());
                        }
                        lastWriteX.get(e.resource).put(e.value, null);

                    }
                    update(e, firstEventinThread);
                    break;

                case EventType.READ:
                    Event lwr = (lastWriteX.get(e.resource) == null) ? null : lastWriteX.get(e.resource).get(e.value);
                    if (lwr != null && !lwr.thread.equals(e.thread) && lwr.value.equals(e.value)) {
                        e.timeStamp.join(lwr.timeStamp);
                        numberOfswEdges++;
                    }
                    update(e, firstEventinThread);

                    break;

                case EventType.JOIN: //join(t,u)
                    long uid = (long) e.resource;
                    TreeClock lastevent = lastInThread.get(uid);
                    if (lastevent != null) {
                        e.timeStamp.join(lastevent);
                        numberOfswEdges++;
                    }
                    update(e, firstEventinThread);
                    break;

                case EventType.FORK:
                    if (e.resource instanceof Thread) {
                        lastFork.put(((Thread) e.resource).getId(), e);
                    }

                    update(e, firstEventinThread);
                    break;

                case EventType.UNLOCK:
                    lastUnlock.put(e.hashedResource, e);
                    update(e, firstEventinThread);
                    break;

                default:
                    update(e, firstEventinThread);
            }

            entered = true;
        }

        return entered;

    }

    private void update(Event e, boolean firstEventinThread) {

        if (firstEventinThread) {
            Event j = lastFork.get(e.thread);
            if (j != null) { //some thread forked me
                long uid = (long) j.resource;
                if (uid == e.thread) {
                    e.timeStamp.join(j.timeStamp);
                    numberOfswEdges++;
                }
            }
        }

        e.timeStamp.incrementBy(1);
        lastInThread.put(e.thread, e.timeStamp);
        if (e.name > 19) //keep only monitoring events
            trace.add(e);
        else
            SAs = SAs + 1;
    }

}
