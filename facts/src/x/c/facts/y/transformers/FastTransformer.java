package x.c.facts.y.transformers;

import x.c.facts.y.ObserverAddress;
import x.y.transformers.Transformer;

import java.util.HashMap;
import java.util.HashSet;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.IntStream;

public abstract class FastTransformer extends Transformer {

    // filter resource by owner / name
    private final HashMap<String, HashSet<String>> staticFilter = new HashMap<>();
    public ObserverAddress observer = new ObserverAddress();

    private boolean filterAccessOnResource(String key, String pattern, boolean precise) {

        if (staticFilter.containsKey(key) && !staticFilter.get(key).isEmpty()) {
            if (precise) {
                return !staticFilter.get(key).contains(pattern);
            } else {

                AtomicBoolean match = new AtomicBoolean(false);
                staticFilter.get(key).stream().forEach(v -> {
                    if (pattern.contains(v)) {
                        match.set(true);
                    }
                });

                return !match.get();
            }
        }
        return false;
    }


    protected boolean filterAccess(String key1, String key2, String value1, String value2, boolean precise) {

        if (!staticFilter.containsKey(key1 + key2)) {

            staticFilter.put(key1 + key2, new HashSet<>());

            if (arguments.containsKey(key1) && arguments.containsKey(key2)) {
                String[] keys1 = arguments.get(key1).split(",");
                String[] keys2 = arguments.get(key2).split(",");
                if (keys1.length == 0 || keys1.length != keys2.length)
                    return false;

                IntStream.range(0, keys1.length)
                        .forEach(index -> staticFilter.get(key1 + key2).add(keys1[index] + keys2[index]));
            } else {
                return false;
            }
        }

        return filterAccessOnResource(key1 + key2, value1 + value2, precise);
    }

    protected boolean filterAccess(String key, String value, boolean precise) {
        if (!staticFilter.containsKey(key)) {
            staticFilter.put(key, new HashSet<>());
            if (arguments.containsKey(key)) {
                String[] keys = arguments.get(key).split(",");
                if (keys.length == 0)
                    return false;

                IntStream.range(0, keys.length)
                        .forEach(index -> staticFilter.get(key).add(keys[index]));
            } else {
                return false;
            }
        }
        return filterAccessOnResource(key, value, precise);
    }

}
