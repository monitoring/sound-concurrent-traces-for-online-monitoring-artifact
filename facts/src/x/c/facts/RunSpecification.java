package x.c.facts;

import java.util.AbstractMap;
import java.util.HashSet;
import java.util.Map;

public class RunSpecification {
    public static HashSet<AbstractMap.SimpleEntry<String, String>> orderDependency;

    public static HashSet<AbstractMap.SimpleEntry<Map<String, Object>, Map<String, Object>>> unOrderedBindings = new HashSet<>();

    public static HashSet<Map<String, Object>> allBindingReceived = new HashSet<>();

    public static int vcAlgorithmSlice = 2000;

    public static boolean turnOffProcessing = false;

    public static boolean turnOffOrderChecking = false;

    public static int numberOfThreads = 5;

    public static boolean parametricEvents = false;
}
