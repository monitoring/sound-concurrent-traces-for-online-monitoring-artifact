package x.c.facts;

import org.xml.sax.SAXException;
import x.y.core.BenchStats;
import x.y.core.BismClassFileTransformer;
import x.y.core.RunArgs;

import javax.xml.parsers.ParserConfigurationException;
import java.lang.instrument.Instrumentation;
import java.util.AbstractMap;
import java.util.HashSet;


public final class Agent {


    public static void premain(String agentArgs, Instrumentation inst) throws ParserConfigurationException, SAXException {

        RunArgs.processArgs(agentArgs);

        if (agentArgs == null) {
            return;
        }

        String[] params = agentArgs.split(":");

        for (String p : params) {

            String[] t = p.split("=");

            if (t[0].equals("processing")) {

                String value = t[1];
                if (value.equalsIgnoreCase("off")) {
                    RunSpecification.turnOffProcessing = true;
                } else {
                    BenchStats.write = true;
                }
            }
            if (t[0].equals("parametric")) {

                String value = t[1];
                if (value.equalsIgnoreCase("off")) {
                    RunSpecification.parametricEvents = true;
                } else {
                    BenchStats.write = true;
                }
            }
            if (t[0].equals("checking")) {

                String value = t[1];
                if (value.equalsIgnoreCase("off")) {
                    RunSpecification.turnOffOrderChecking = true;
                }
            }

            if (t[0].equals("threads")) {

                try {
                    int value = Integer.parseInt(t[1]);
                    RunSpecification.numberOfThreads = value;
                } catch (Exception e) {
                    System.out.println("Default thread number");
                }

            }

            if (t[0].equals("dependency")) {

                if (RunSpecification.orderDependency == null) {
                    RunSpecification.orderDependency = new HashSet<>();
                }

                //  We convert from "A--B+C--D" to (A,B),(C,D)

                if (t.length < 2) {
                    continue;
                }

                String value = t[1];

                if (value == null || value.isEmpty()) {

                    continue;
                }

                String[] keyValuePairs = value.split("--");

                for (String pair : keyValuePairs) {

                    String[] entry = pair.split("\\+");

                    RunSpecification.orderDependency.add(new AbstractMap.SimpleEntry<>(entry[0].trim(), entry[1].trim()));
                }
            }
        }

        System.out.println("Dependencies: " + RunSpecification.orderDependency);

        inst.addTransformer(new BismClassFileTransformer(),
                true);
    }

}

