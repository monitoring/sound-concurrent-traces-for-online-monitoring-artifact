package x.c.facts.observation;

import x.c.facts.Synchronizer;
import x.c.facts.events.Event;

import java.util.ArrayList;
import java.util.List;

/***
 * Observer for AA
 */
public class Observer3 {


    public static List
            obs = new ArrayList();


    public static void observe(int name, long thread, Object resource, String resourceName, Object value) {

        try {
            if (resource == null || resourceName == null)
                return;

            Event e = new Event(name, thread, System.identityHashCode(resource) + System.identityHashCode(resourceName), System.identityHashCode(value));

            addevent(e);
        } catch (Exception x) {
            boolean br = resource == null;
            System.out.println(br);
        }
    }

    private static void addevent(Event e) {
        synchronized (Synchronizer.myLock) {
            obs.add(e);
        }
    }

    public static void observe(int name, long thread, Object resource, Object resourceName, Object value) {

        if (resource == null || resourceName == null)
            return;

        Event e = new Event(name, thread, System.identityHashCode(resource) + System.identityHashCode(resourceName), System.identityHashCode(value));
        addevent(e);

    }

    public static void observe(int name, long thread, String address, Object resource) {

        addevent(new Event(name, thread, address, System.identityHashCode(resource), 0));
    }

    public static void observe(int name, long thread, Object resource, Object value) {

        addevent(new Event(name, thread, System.identityHashCode(resource), System.identityHashCode(value)));
    }


    public static void observe(int name, long thread, Object resource) {
        addevent(new Event(name, thread, System.identityHashCode(resource), 0));
    }

    public static void observe(int name, long thread, String resource) {
        addevent(new Event(name, thread, System.identityHashCode(resource), 0));
    }


    public static void observe(int name, long thread) {
        obs.add(new Event(name, thread, 0, 0));
    }

    public static void observe(int name, long thread, String address, List<String> variableNames, List<Object> variableBindings) {
        Event e = new Event(name, thread, address, variableNames, variableBindings);
        addevent(e);
    }

    public static void observe(int name, long thread, Object resource, List<String> variableNames, List<Object> variableBindings) {
        Event e = new Event(name, thread, System.identityHashCode(resource), variableNames, variableBindings);
        addevent(e);
    }

}
