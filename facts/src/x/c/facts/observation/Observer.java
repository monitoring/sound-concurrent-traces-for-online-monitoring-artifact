package x.c.facts.observation;

import x.c.facts.events.Event;

import java.util.List;

/***
 * Observer for facts
 */
public class Observer {

    private static final Observation obs = Observation.getInstance();

    public static void observe(int name, long thread, Object resource, String resourceName, Object value) {

        try {
            if (resource == null || resourceName == null)
                return;

            Event e = new Event(name, thread, resource + "." + resourceName, value);

            obs.addEvent(e);
        } catch (Exception x) {
            boolean br = resource == null;
            System.out.println(br);
        }
    }

    public static void observe(int name, long thread, Object resource, Object resourceName, Object value) {

        if (resource == null || resourceName == null)
            return;

        Event e = new Event(name, thread, resource + "." + resourceName, value);
        obs.addEvent(e);

    }

    public static void observe(int name, long thread, String address, Object resource) {
        Event e = new Event(name, thread, address, resource, null);

        obs.addEvent(e);
    }

    public static void observe(int name, long thread, Object resource, Object value) {

        obs.addEvent(new Event(name, thread, resource, value));
    }

    public static void observe(int name, long thread, Object resource) {

        int res = System.identityHashCode(resource);

        if (res == 0) {
            System.out.println("Resource zero");
        }

        obs.addEvent(new Event(name, thread, res));
    }

    public static void observe(int name, long thread, Object resource, int y) {

        if (name == 7 || name == 8) {
            if (resource instanceof Thread) {
                obs.addEvent(new Event(name, thread, ((Thread) resource).getId()));
            }
        } else
            obs.addEvent(new Event(name, thread, resource));
    }


    public static void observe(int name, long thread, String resource) {

        int res = System.identityHashCode(resource);
        if (res == 0) {
            System.out.println("Resource zero");
        }

        Event e = new Event(name, thread, res);
        obs.addEvent(e);
    }


    public static void observe(int name, long thread) {

        obs.addEvent(new Event(name, thread, null, null));
    }

    public static void observe(int name, long thread, String address, List<String> variableNames, List<Object> variableBindings) {
        Event e = new Event(name, thread, address, variableNames, variableBindings);
        obs.addEvent(e);
    }

    public static void observe(int name, long thread, Object resource, List<String> variableNames, List<Object> variableBindings) {
        Event e = new Event(name, thread, resource, variableNames, variableBindings);
        obs.addEvent(e);
    }

    public static void finish() {

        System.out.println("Finished Observing");
        obs.finish();

    }

}
