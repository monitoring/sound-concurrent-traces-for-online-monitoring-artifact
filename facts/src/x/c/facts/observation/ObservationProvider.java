package x.c.facts.observation;

public class ObservationProvider {


    private static Observation dbObject;

    private ObservationProvider() {
    }

    public static Observation getInstance() {

        // create object if it's not already created
        if (dbObject == null) {
            dbObject = new Observation();
        }
        // returns the singleton object
        return dbObject;
    }

}
