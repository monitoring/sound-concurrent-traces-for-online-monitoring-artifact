package x.c.facts.observation;

import x.c.facts.events.Event;

import java.util.ArrayList;
import java.util.List;

/***
 * Observer for FA
 */
public class Observer2 {


    public static List
            obs = new ArrayList();


    public static void observe(int name, long thread, Object resource, String resourceName, Object value) {

        try {
            if (resource == null || resourceName == null)
                return;

            Event e = new Event(name, thread, System.identityHashCode(resource) + System.identityHashCode(resourceName), System.identityHashCode(value));

            obs.add(e);
        } catch (Exception x) {
            boolean br = resource == null;
            System.out.println(br);
        }
    }

    public static void observe(int name, long thread, Object resource, Object resourceName, Object value) {

        if (resource == null || resourceName == null)
            return;

        Event e = new Event(name, thread, System.identityHashCode(resource) + System.identityHashCode(resourceName), System.identityHashCode(value));
        obs.add(e);

    }

    public static void observe(int name, long thread, String address, Object resource) {

        obs.add(new Event(name, thread, address, System.identityHashCode(resource), 0));
    }

    public static void observe(int name, long thread, Object resource, Object value) {

        obs.add(new Event(name, thread, System.identityHashCode(resource), System.identityHashCode(value)));
    }


    public static void observe(int name, long thread, Object resource) {
        obs.add(new Event(name, thread, System.identityHashCode(resource), 0));
    }

    public static void observe(int name, long thread, String resource) {
        obs.add(new Event(name, thread, System.identityHashCode(resource), 0));
    }


    public static void observe(int name, long thread) {
        obs.add(new Event(name, thread, 0, 0));
    }

    public static void observe(int name, long thread, String address, List<String> variableNames, List<Object> variableBindings) {
        Event e = new Event(name, thread, address, variableNames, variableBindings);
        obs.add(e);
    }

    public static void observe(int name, long thread, Object resource, List<String> variableNames, List<Object> variableBindings) {
        Event e = new Event(name, thread, System.identityHashCode(resource), variableNames, variableBindings);
        obs.add(e);
    }

}
