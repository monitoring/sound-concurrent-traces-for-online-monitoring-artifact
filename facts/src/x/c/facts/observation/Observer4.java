package x.c.facts.observation;

import x.y.core.BenchStats;

import java.text.AttributedCharacterIterator;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/***
 * To be deleted
 */
public class Observer4 {

    public static List
            obs = new ArrayList();


    public static int eventsCounter = 0;
    public static int nullCounter = 0;


    public static List<Integer> typesEmitted = new ArrayList<>();

    public static void saveType(Object b) {

        typesEmitted.add(1);
        nullCounter = typesEmitted.size();

    }

    public static void finish() {

        System.out.println("Finished Observing");

        System.out.println(BenchStats.print());

        System.out.println("Events received:" + eventsCounter);

    }


    public synchronized static void incrementCounter() {

        eventsCounter++;

    }

    public static void count(int x) {
        incrementCounter();
    }


    public static void observe(Object i) {

        incrementCounter();
        saveType(i);
    }


    public static void observe(int i) {

        incrementCounter();
        saveType(i);

    }


    public static void observe(AttributedCharacterIterator i, Object s) {

    }

    public static void observe(Object i, Object s) {

        incrementCounter();
        saveType(i);
        saveType(s);

    }

    public static void observe(ListIterator i, Object s) {

        incrementCounter();

        saveType(i);
        saveType(s);

    }


    public static void observe(Iterator i, Object s) {

    }

}
