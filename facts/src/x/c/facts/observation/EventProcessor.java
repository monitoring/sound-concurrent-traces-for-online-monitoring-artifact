package x.c.facts.observation;


import x.c.facts.RunSpecification;
import x.c.facts.events.Event;
import x.c.facts.tracechecker.OrderChecker;
import x.c.facts.vectorclock.algorithm.VectorClockAlgorithm;

import java.util.List;

/***
 * Responsible for running the vector clock and order checker algorithm.
 */
public class EventProcessor implements Runnable {


    private final VectorClockAlgorithm algorithmInstance;
    private final OrderChecker orderChecker;
    private final List<Event> trace;

    volatile boolean finishedObserving = false;


    public EventProcessor(VectorClockAlgorithm vca, OrderChecker orderChecker, List<Event> trace) {

        this.algorithmInstance = vca;
        this.orderChecker = orderChecker;
        this.trace = trace;

    }

    @Override
    public void run() {

        long start;
        start = System.currentTimeMillis();

        if (!RunSpecification.turnOffProcessing) {

            while (true) {
                boolean vcaDidSomeWork = algorithmInstance.processEntry();

                //TODO: this can be enhanced
                if (!vcaDidSomeWork && finishedObserving) {
                    break;
                }
                try {
                    Thread.sleep(1500);
                    start += 1500;
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("VCA Ordering finished in " + (System.currentTimeMillis() - start) + " mss");

        } else {
            System.out.println("VCA Ordering is off");
        }

        if (!RunSpecification.turnOffOrderChecking) {
            start = System.currentTimeMillis();
            while (true) {

                try {
                    Thread.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                boolean orderCheckerDidSomeWork = false;
                if (RunSpecification.parametricEvents)
                    orderCheckerDidSomeWork = orderChecker.findParametricMissingOrders();
                else
                    orderCheckerDidSomeWork = orderChecker.findMissingOrders();

                //TODO: this can be enhanced
                if (!orderCheckerDidSomeWork && finishedObserving) {
                    break;
                }
            }
            System.out.println("Trace Checking finished in " + (System.currentTimeMillis() - start) + " mss");
        } else {
            System.out.println("Trace Checking is off");
        }

        return;

    }
}
