package x.c.facts.observation;


import x.c.facts.RunSpecification;
import x.c.facts.events.Event;
import x.c.facts.graph.GraphHelpers;
import x.c.facts.tracechecker.OrderChecker;
import x.c.facts.vectorclock.algorithm.VectorClockAlgorithm;

import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;


public class Observation {

    private static Observation obs;
    private final Queue<Event> queue;
    private final List<Event> trace;
    private final Object myLock = new Object();
    private final VectorClockAlgorithm vca;
    private final OrderChecker orderChecker;
    private final EventProcessor eventProcessor;
    private final Thread eventProcessorThread;
    private final boolean draw = true;
    private final GraphHelpers gf = null;
    private final GraphHelpers gr = null;
    public HashSet<String> threads = new HashSet<>();
    int i = 0;
    private int eventsReceived;

    public Observation() {

        queue = new ConcurrentLinkedQueue<Event>();
        trace = new ArrayList<>();

        orderChecker = new OrderChecker(RunSpecification.orderDependency, trace);
        vca = new VectorClockAlgorithm(queue, trace);

        eventProcessor = new EventProcessor(vca, orderChecker, trace);
        eventProcessorThread = new Thread(eventProcessor);
        eventProcessorThread.start();

    }

    public synchronized static Observation getInstance() {

        // create object if it's not already created
        if (obs == null) {
            obs = new Observation();
        }

        // returns the singleton object
        return obs;
    }

    public void addEvent(Event e) {
        if (e == null) return;
        queue.add(e);

    }

    public void finish() {

        eventProcessor.finishedObserving = true;

        try {
            eventProcessorThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        Collection<Event> c = queue;

        if (RunSpecification.turnOffProcessing) {
            c = queue;
        } else {
            c = trace;
        }

        System.out.println("Total Number of Events: " + (c.size() + Observer2.obs.size() + Observer3.obs.size()));

        long RAs = c.stream().filter(t -> t.name >= 20).count() + Observer2.obs.size() + Observer3.obs.size();
        long sacount = ((c.size() + Observer2.obs.size() + Observer3.obs.size()) - RAs) + vca.SAs;
        System.out.println("SAs count " + sacount);
        System.out.println("RAs count " + RAs);

        if (!RunSpecification.turnOffProcessing) {
            System.out.println("Total number of Threads: " + vca.seenThreads.size());
            System.out.println("Total Number of Synchronization Edges: " + vca.numberOfswEdges);
            System.out.println("Total Number of Wrong Orderings: " + orderChecker.unOrderedPairsCount);
            System.out.println("Total Ordered Pairs from different Threads: " + orderChecker.orderedPairsCount);
        }

        System.exit(0);

    }

}
