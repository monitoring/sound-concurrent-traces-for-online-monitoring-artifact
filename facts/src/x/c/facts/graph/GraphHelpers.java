package x.c.facts.graph;

import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.SingleGraph;
import org.graphstream.ui.view.Viewer;
import x.c.facts.events.Event;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class GraphHelpers {

    public Graph graph;
    protected Map<Long, Integer[]> gridMap = new HashMap<>();
    private int y = 0;


    public GraphHelpers() {
        graph = new SingleGraph("Tutorial 1");

        System.setProperty("org.graphstream.ui", "swing");
        //graph.setAttribute("ui.stylesheet", "url(file:///Users/qqqq/Src/tools/vector-clock/resources/graph.css')");
        graph.setAttribute("ui.stylesheet", "graph {  \n" +
                "\n" +
                "}\n" +
                "\n" +
                "node {\n" +
                "    size: 12px;\n" +
                "    fill-mode: plain;   /* Default.          */\n" +
                "    fill-color: black;    /* Default is black. */\n" +
                "    stroke-mode: plain; /* Default is none.  */\n" +
                "    stroke-color: black; /* Default is black. */\n" +
                "\tstroke-width: 1px;\n" +
                "\ttext-size: 9px;\n" +
                "\ttext-alignment: above;\n" +
                "\ttext-color: black;\n" +
                "\ttext-mode: normal;\n" +
                "\n" +
                "}\n" +
                "\n" +
                " node.t0 {  fill-color: orange;  } \n" +
                " node.t1 {  fill-color: green;} \n" +
                " node.t2 {  fill-color: red;  } \n" +
                " node.t3 {  fill-color: blue;} \n" +
                " node.t4 {  fill-color: grey;  } \n" +
                " node.t5 {  fill-color: yellow;} \n" +
                " node.t6 {  fill-color: purple;  } \n" +
                " node.t7 {  fill-color: black;} \n" +
                " node.t8 {  fill-color: red;  } \n" +
                " \n" +
                "\n" +
                " node.pool2thread1 { fill-color: green; } \n" +
                "node.pool2thread2 {  fill-color: red;  } \n" +
                "node.pool2thread3 {  fill-color: blue;  } \n" +
                "node.pool2thread4 {  fill-color: yellow;  } \n" +
                "node.pool2thread5 {  fill-color: brown;  } \n" +
                "\n" +
                "node.marked  { fill-color: purple; size: 10px; }\n" +
                "node.pushed  { fill-color: #1b9628; size: 20px; }\n" +
                "\n" +
                "edge {\n" +
                "\tarrow-shape: arrow;\n" +
                "\tarrow-size: 5px;\n" +
                "\tstroke-width: 0.5px;\n" +
                "\tstroke-mode: plain;\n" +
                "\ttext-size: 3;\n" +
                "\ttext-background-mode: rounded-box;\n" +
                "\ttext-background-color: white;\n" +
                "}");

        //  graph.setAttribute("ui.stylesheet", "url('" + VCAlgorithm.class.getResource("/graph.css") + "')");
        graph.setAttribute("ui.quality");
        graph.setAttribute("ui.antialias");

        Viewer viewer = graph.display();
        viewer.disableAutoLayout();

    }

    public void pushNode(Event e, boolean fullLabel) {

        Node node = graph.addNode(e.toString());
        Long thread = e.thread;
//        String label = fullLabel? e.toStringWithVC() : "t" + thread + "." + e.address;
        String label = fullLabel ? e.toStringWithVC() : "t" + thread;
        Integer[] coords = gridMap.get(thread);
        if (coords == null) {
            coords = new Integer[]{0, y};
            y++;
            gridMap.put(thread, coords);
        } else
            coords[0]++;

        int[] newCoords = new int[]{coords[0].intValue(), coords[1].intValue()};

        node.setAttribute("ui.label", label);
        node.setAttribute("ui.class", "t" + thread % 9);
        node.setAttribute("xy", newCoords);

    }

    public void fullGraphDisplay(List<Event> orderedTrace, List<Map.Entry<Event, Event>> swEdges) {

        for (Event e : orderedTrace) {
            this.pushNode(e, true);
        }

        Collection<List<Event>> output = orderedTrace.stream()
                .collect(Collectors.groupingBy(Event::getThread))
                .values();

        output.forEach(s -> IntStream.range(1, s.size())
                .mapToObj(i -> new AbstractMap.SimpleEntry<>(s.get(i - 1), s.get(i)))
                .forEach(h -> graph.addEdge(UUID.randomUUID().toString(), h.getKey().toString(), h.getValue().toString(), true))
        );

        swEdges.forEach(s -> graph.addEdge(UUID.randomUUID().toString(), s.getKey().toString(), s.getValue().toString(), true));

    }


    public void relevantGraphDisplay(HashSet<String> eventNames, boolean displayThreadOrder, List<Event> orderedTrace) {

        List<Event> EE = new ArrayList<>();

        orderedTrace.forEach(e -> {
            if (eventNames.contains(e.name)) {
                pushNode(e, false);
                EE.add(e);
            }
        });

        Collection<List<Event>> output = EE.stream()
                .collect(Collectors.groupingBy(Event::getThread))
                .values();

        if (displayThreadOrder) {
            output.forEach(s -> IntStream.range(1, s.size())
                    .mapToObj(i -> new AbstractMap.SimpleEntry<>(s.get(i - 1), s.get(i)))
                    .forEach(h -> graph.addEdge(UUID.randomUUID().toString(), h.getKey().toString(), h.getValue().toString(), true))
            );
        }

        HashMap<Event, List<Event>> incomingEdges = new HashMap<>();

        for (int k = 0; k < EE.size(); k++) {
            Event from = EE.get(k);

            for (int l = 0; l < EE.size(); l++) {

                Event to = EE.get(l);
                if (!incomingEdges.containsKey(to)) {

                    incomingEdges.put(to, new ArrayList<>());
                }

                if (!from.thread.equals(to.thread)) {

                    if (from.timeStamp.isLessThanOrEqual(to.timeStamp)) {
                        List edges = incomingEdges.get(to);
                        edges.add(from);
                        incomingEdges.put(to, edges);

                        //graph.addEdge(UUID.randomUUID().toString(), from.toString(), to.toString(), true);
                    }

                }
            }
        }

        HashMap<Event, List<Event>> incomingEdgesFiltered = new HashMap<>();

        for (Event to : incomingEdges.keySet()) {

            List<Event> incoming = incomingEdges.get(to);

            List<Event> toremove = new ArrayList<>();
            if (!incoming.isEmpty()) {
                Event maxEvent = incoming.get(0);

                incoming.forEach(e1 -> incoming.forEach(e2 -> {
                    if (e1 != e2) {
                        if (e1.timeStamp.isLessThanOrEqual(e2.timeStamp)) {
                            toremove.add(e1);
                        }
                    }
                }));

                incoming.removeAll(toremove);
                incomingEdgesFiltered.put(to, incoming);
            }
        }

        incomingEdgesFiltered.forEach((s, v) -> {
            v.forEach(ff -> graph.addEdge(UUID.randomUUID().toString(), ff.toString(), s.toString(), true));
        });

    }

}
